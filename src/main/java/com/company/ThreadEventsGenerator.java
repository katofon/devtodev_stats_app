package com.company;
import com.company.baseClasses.User;
import com.company.managers.UsersManager;

public class ThreadEventsGenerator extends Thread {
    public static final int THREADS_COUNT = 3;
    public static final int EVENTS_COUNT_IN_THREAD = 500;
    public static final int EVENTS_TYPES_COUNT = 3;

    public ThreadEventsGenerator(String name) {
        super(name);
    }

    public static void initEventsGeneration() {
        for (int i = 0; i < THREADS_COUNT; i++) {
            ThreadEventsGenerator t = new ThreadEventsGenerator("thread" + i);
            t.start();
        }
    }

    public void run() {
        try {
            for (int i = 0; i < EVENTS_COUNT_IN_THREAD; i++) {
                User randomUser = UsersManager.instance.getRandomUser();

                if (randomUser == null)
                    return;

                switch(Helper.RANDOM.nextInt(EVENTS_TYPES_COUNT)) {
                    case 0:
                        randomUser.levelUp();
                        break;
                    case 1:
                        randomUser.buyRandomItem();
                        break;
                    case 2:
                        randomUser.sessionEnd();
                        break;
                }
                Thread.sleep(Helper.RANDOM.nextInt(5));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
