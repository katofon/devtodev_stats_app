package db;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBConnection {
    private static DataSource dataSource;

    static {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:postgresql://127.0.0.1/statistics_app");
        ds.setUsername("katofon");
        ds.setMaximumPoolSize(80);
        dataSource = ds;
    }

    static Connection getConnection() throws Exception {
        return dataSource.getConnection();
    }

    public static ResultSet executeQuery(String query) throws Exception {
        Statement stmt = null;

        try {
            stmt = getConnection().createStatement();
            return stmt.executeQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static int executeUpdate(String query) throws Exception {
        Statement stmt = null;

        try {
            stmt = getConnection().createStatement();
            return stmt.executeUpdate(query);
        } finally {
            if (stmt != null) {
                stmt.getConnection().close();
            }
        }
    }
}