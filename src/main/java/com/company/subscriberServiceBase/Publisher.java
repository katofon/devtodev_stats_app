package com.company.subscriberServiceBase;
import java.util.ArrayList;
import java.util.List;

public abstract class Publisher {
    private List<SubscriberInterface> subscribers = new ArrayList<SubscriberInterface>();

    public void addSubscriber(SubscriberInterface subscriber) {
        subscribers.add(subscriber);
    }

    public void notifySubscribers(PublisherEventInterface event) {
        for (SubscriberInterface subscriber: subscribers) {
            subscriber.handleEvent(event);
        }
    }

}
