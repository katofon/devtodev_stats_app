package com.company.subscriberServiceBase;

public interface SubscriberInterface {
    public void handleEvent(PublisherEventInterface event);
}
