package com.company.scheduleService.jobs;
import com.company.scheduleService.StatsJob;
import db.DBConnection;
import org.joda.time.DateTime;
import org.quartz.*;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class LevelStatsJob extends StatsJob {
    protected String fileName() {
        return "level_stats.csv";
    }

    private static final int LEVEL = 42;
    private static final String SELECT_LEVEL_PERIOD = "'1 hour'";
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            Timestamp now = new Timestamp(new DateTime().getMillis());
            ResultSet rs = DBConnection.executeQuery(
                    "SELECT COUNT(user_id) " +
                            "FROM level_up_stats " +
                            "WHERE new_level = " + LEVEL + " " + "AND user_id NOT IN (" +
                            "SELECT DISTINCT user_id " +
                            "FROM level_up_stats " +
                            "WHERE new_level = " + (LEVEL + 1) + " " + "AND date < timestamp '" + now + "' - interval " + SELECT_LEVEL_PERIOD +
                            ")"
            );

            if(rs.next()) {
                String[] record = {now.toString(), rs.getString("count")};
                List<String[]> recordsList = new ArrayList<String[]>();
                recordsList.add(record);
                writeCSV(recordsList);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
