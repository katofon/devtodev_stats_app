package com.company.scheduleService.jobs;
import com.company.scheduleService.StatsJob;
import db.DBConnection;
import org.joda.time.DateTime;
import org.quartz.*;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BuyItemStatsJob extends StatsJob {

    protected String fileName() {
        return "buy_stats.csv";
    }

    private static final String SELECT_BUY_PERIOD = "'1 hour'";
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            Timestamp now = new Timestamp(new DateTime().getMillis());
            ResultSet rs = DBConnection.executeQuery(
                    "SELECT item_id, SUM(item_price) " +
                            "FROM buy_stats " +
                            "WHERE date > timestamp '" + now + "' - interval " + SELECT_BUY_PERIOD + " " +
                            "GROUP BY item_id"
            );

            List<String[]> recordsList = new ArrayList<String[]>();
            while(rs.next()) {
                String[] record = {now.toString(), rs.getString("item_id"), rs.getString("sum")};
                recordsList.add(record);
            }
            writeCSV(recordsList);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
