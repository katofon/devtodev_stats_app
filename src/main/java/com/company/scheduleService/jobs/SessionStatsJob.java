package com.company.scheduleService.jobs;
import com.company.scheduleService.StatsJob;
import db.DBConnection;
import org.joda.time.DateTime;
import org.quartz.*;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SessionStatsJob extends StatsJob {

    protected String fileName() {
        return "session_stats.csv";
    }

    private static final String SELECT_SESSION_PERIOD = "'5 minutes'";
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            Timestamp now = new Timestamp(new DateTime().getMillis());
            ResultSet rs = DBConnection.executeQuery(
                    "SELECT COUNT(DISTINCT user_id) " +
                            "FROM session_length_stats " +
                            "WHERE date > timestamp '" + now + "' - interval " + SELECT_SESSION_PERIOD
            );

            if(rs.next()) {
                String[] record = {now.toString(), rs.getString("count")};
                List<String[]> recordsList = new ArrayList<String[]>();
                recordsList.add(record);
                writeCSV(recordsList);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
