package com.company.scheduleService;
import au.com.bytecode.opencsv.CSVWriter;
import org.quartz.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public abstract class StatsJob implements Job {
    public void execute(JobExecutionContext arg0) throws JobExecutionException { }

    protected void writeCSV(List<String[]> recordsList)  {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(System.getProperty("user.dir") + "/src/csv/" + fileName(), true));
            writer.writeAll(recordsList);
            writer.close();
        } catch(IOException e) {
            System.out.println(e.toString());
        }
    }

    protected String fileName() {
        return null;
    }
}
