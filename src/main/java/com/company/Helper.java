package com.company;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Random;

public class Helper {
    public static final Random RANDOM = new Random();

    public static String generateRandomIntString(int length) {
        String res = "";
        for (int i = 0; i < length; i++) {
            res += String.valueOf(RANDOM.nextInt(10));
        }

        return  res;
    }

    public static Object getRandomArrayElement(ArrayList array) {
        return array.get(RANDOM.nextInt(array.size()));
    }

    public static final int RANDOM_PERIOD_IN_HOURS = 3;
    public static final long PERIOD_TIME_START = new DateTime().minusHours(RANDOM_PERIOD_IN_HOURS).getMillis();
    public static final long PERIOD_TIME_END = new DateTime().getMillis();
    public static final int TIME_SHIFT = (int) (PERIOD_TIME_END - PERIOD_TIME_START)/(ThreadEventsGenerator.THREADS_COUNT * ThreadEventsGenerator.EVENTS_COUNT_IN_THREAD);
    public static long currentTime = PERIOD_TIME_START;
    public static Timestamp generateRandomDateTime() {
        long start = currentTime;
        long end = currentTime + TIME_SHIFT;
        Timestamp ts = new Timestamp((RANDOM.nextInt( (int)((end - start) + 1) ) + start));
        currentTime = end;
        return ts;
    }
}
