package com.company.managers;
import com.company.baseClasses.User;
import com.company.Helper;
import java.util.ArrayList;

public class UsersManager {
    public static final int USERS_COUNT = 10;
    public static final int USER_IDENTIFIER_LENGTH = 64;

    public final static UsersManager instance = new UsersManager();

    private ArrayList<User> _usersArray = new ArrayList<User>();

    public void initUsers() {
        for (int i = 0; i < USERS_COUNT; i++) {
            _usersArray.add(new User(Helper.generateRandomIntString(USER_IDENTIFIER_LENGTH)));
        }
    }

    public ArrayList<User> getUseraArray() {
        return _usersArray;
    }

    public User getRandomUser() {
        if (_usersArray.size() == 0)
            return null;

        return (User) Helper.getRandomArrayElement(_usersArray);
    }
}
