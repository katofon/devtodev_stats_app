package com.company.managers;
import com.company.baseClasses.Item;
import com.company.Helper;
import java.util.ArrayList;

public class ItemsManager {
    public static final int MAX_ITEM_PRICE = 60000;
    public static final int ITEMS_COUNT = 10;
    public final static ItemsManager instance = new ItemsManager();

    private ArrayList<Item> _itemsArray = new ArrayList<Item>();

    public void initRandomItems() {
        for (int i = 0; i < ITEMS_COUNT; i++) {
            _itemsArray.add(new Item(i, Helper.RANDOM.nextInt(MAX_ITEM_PRICE)));
        }
    }

    public Item getRandomItem() {
        if (_itemsArray.size() == 0)
            return null;

        return (Item) Helper.getRandomArrayElement(_itemsArray);
    }

}
