package com.company.managers;
import com.company.scheduleService.jobs.BuyItemStatsJob;
import com.company.scheduleService.jobs.LevelStatsJob;
import com.company.scheduleService.jobs.SessionStatsJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class ScheduleManager {
    public static final int SELECT_INTERVAL_IN_SECONDS = 3;
    public static final Class  [] JOBS = {
            LevelStatsJob.class,
            SessionStatsJob.class,
            BuyItemStatsJob.class
    };

    public static void init() {
        try {
            Scheduler sc = StdSchedulerFactory.getDefaultScheduler();
            sc.start();

            for (Class jobClass:JOBS) {
                JobDetail job = JobBuilder.newJob(jobClass).build();
                Trigger t = TriggerBuilder.newTrigger().withIdentity(jobClass.getName()).withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(SELECT_INTERVAL_IN_SECONDS).repeatForever()).build();
                sc.scheduleJob(job, t);
            }
        } catch(SchedulerException e) {
            System.out.println(e.getMessage());
        }
    }
}
