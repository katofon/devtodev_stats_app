package com.company.managers;
import com.company.statsService.StatsEventBase;
import com.company.subscriberServiceBase.Publisher;

public class StatsManager extends Publisher {
    public final static StatsManager instance = new StatsManager();

    public void sendStats(StatsEventBase event) {
        notifySubscribers(event);
    }
}



