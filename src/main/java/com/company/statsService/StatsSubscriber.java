package com.company.statsService;
import com.company.statsService.events.StatsEventBuy;
import com.company.statsService.events.StatsEventLevelUp;
import com.company.statsService.events.StatsEventSessionEnd;
import com.company.subscriberServiceBase.PublisherEventInterface;
import com.company.subscriberServiceBase.SubscriberInterface;
import db.DBConnection;
import java.sql.Timestamp;

public class StatsSubscriber implements SubscriberInterface {
    public void handleEvent(PublisherEventInterface event) {
        try {
            if (event instanceof StatsEventBuy) {
                StatsEventBuy evt = (StatsEventBuy) event;
                DBConnection.executeUpdate(
                "INSERT INTO " +
                        "buy_stats (user_id, item_id, item_price, date) " +
                        "VALUES (" +
                            evt.getUserIdentifier() + "," +
                            evt.getItemId() + "," +
                            evt.getItemPrice() + "," +
                            "'" + evt.getDateTime() + "'" +
                        ")"
                );
            } else if (event instanceof StatsEventLevelUp) {
                StatsEventLevelUp evt = (StatsEventLevelUp) event;
                DBConnection.executeUpdate(
                "INSERT INTO " +
                        "level_up_stats (user_id, new_level, date) " +
                        "VALUES (" +
                            evt.getUserIdentifier() + "," +
                            evt.getNewLevel() + "," +
                            "'" + evt.getDateTime() + "'" +
                        ")"
                );
            } else if (event instanceof StatsEventSessionEnd) {
                StatsEventSessionEnd evt = (StatsEventSessionEnd) event;
                DBConnection.executeUpdate(
                "INSERT INTO " +
                        "session_length_stats (user_id, session_length, date) " +
                        "VALUES (" +
                            evt.getUserIdentifier() + "," +
                            evt.getSessionLength() + "," +
                            "'" + evt.getDateTime() + "'" +
                        ")"
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
