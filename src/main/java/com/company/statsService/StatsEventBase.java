package com.company.statsService;
import com.company.subscriberServiceBase.PublisherEventInterface;

import java.sql.Timestamp;

public abstract class StatsEventBase implements PublisherEventInterface {
    protected String _userIdentifier;
    protected Timestamp _dateTime;

    public StatsEventBase(String userIdentifier, Timestamp dateTime) {
        _userIdentifier = userIdentifier;
        _dateTime = dateTime;
    }

    public String getUserIdentifier() {
        return _userIdentifier;
    }

    public Timestamp getDateTime() {
        return _dateTime;
    }
}
