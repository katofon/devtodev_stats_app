package com.company.statsService.events;
import com.company.statsService.StatsEventBase;

import java.sql.Timestamp;

public class StatsEventBuy extends StatsEventBase {
    protected int _itemId;
    protected int _itemPrice;

    public StatsEventBuy(String userIdentifier, Timestamp dateTime, int itemId, int itemPrice) {
        super(userIdentifier, dateTime);
        _itemId = itemId;
        _itemPrice = itemPrice;
    }

    public int getItemId() {
        return _itemId;
    }

    public int getItemPrice() {
        return _itemPrice;
    }
}
