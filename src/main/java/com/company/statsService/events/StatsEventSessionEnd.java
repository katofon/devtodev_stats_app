package com.company.statsService.events;
import com.company.statsService.StatsEventBase;

import java.sql.Timestamp;

public class StatsEventSessionEnd extends StatsEventBase {
    protected int _sessionLength;

    public StatsEventSessionEnd(String userIdentifier, Timestamp dateTime, int sessionLength) {
        super(userIdentifier, dateTime);
        _sessionLength = sessionLength; //seconds
    }

    public int getSessionLength() {
        return _sessionLength;
    }
}
