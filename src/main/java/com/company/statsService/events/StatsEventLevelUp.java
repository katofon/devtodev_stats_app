package com.company.statsService.events;
import com.company.statsService.StatsEventBase;

import java.sql.Timestamp;

public class StatsEventLevelUp extends StatsEventBase {
    protected int _newLevel;

    public StatsEventLevelUp(String userIdentifier, Timestamp dateTime, int newLevel) {
        super(userIdentifier, dateTime);
        _newLevel = newLevel;
    }

    public int getNewLevel() {
        return _newLevel;
    }
}
