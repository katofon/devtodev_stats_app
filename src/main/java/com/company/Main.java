package com.company;
import com.company.managers.ItemsManager;
import com.company.managers.ScheduleManager;
import com.company.managers.StatsManager;
import com.company.managers.UsersManager;
import com.company.statsService.StatsSubscriber;

public class Main {

    public static void main(String[] args) {
        ItemsManager.instance.initRandomItems();
        UsersManager.instance.initUsers();
        StatsManager.instance.addSubscriber(new StatsSubscriber());
        ThreadEventsGenerator.initEventsGeneration();
        ScheduleManager.init();
    }
}



