package com.company.baseClasses;
import com.company.managers.ItemsManager;
import com.company.Helper;
import com.company.managers.StatsManager;
import com.company.statsService.events.StatsEventBuy;
import com.company.statsService.events.StatsEventLevelUp;
import com.company.statsService.events.StatsEventSessionEnd;
import org.joda.time.Hours;

public class User {
    private String _identifier;
    private int _level = 1;

    public User(String identifier) {
        _identifier = identifier;
    }

    public void levelUp() {
        _level++;
        StatsManager.instance.sendStats(new StatsEventLevelUp(_identifier, Helper.generateRandomDateTime(), _level));
    }

    public void buyRandomItem() {
        Item randomItem = ItemsManager.instance.getRandomItem();
        if (randomItem == null)
            return;

        StatsManager.instance.sendStats(new StatsEventBuy(_identifier, Helper.generateRandomDateTime(), randomItem.getId(), randomItem.getPrice()));
    }

    public void sessionEnd() {
        StatsManager.instance.sendStats(new StatsEventSessionEnd(_identifier, Helper.generateRandomDateTime(), Helper.RANDOM.nextInt(Hours.ONE.toStandardSeconds().getSeconds())));
    }
}



