package com.company.baseClasses;

public class Item {
    private int _price;
    private int _id;

    public Item(int id, int price) {
        _price = price;
        _id = id;
    }

    public int getPrice() { return _price; }

    public int getId() { return _id; }
}



