create table buy_stats (
    user_id text not null,
    item_id int not null,
    item_price int not null,
    date timestamp not null
);