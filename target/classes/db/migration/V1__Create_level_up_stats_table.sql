create table level_up_stats (
    user_id text not null,
    new_level int not null,
    date timestamp not null
);