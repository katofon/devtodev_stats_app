create table session_length_stats (
    user_id text not null,
    session_length int not null,
    date timestamp not null
);